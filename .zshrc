export GOBIN=$HOME/go/bin
export GO111MODULE=auto
export GOPRIVATE=gitlab.com
export PATH=$PATH:$HOME/bin:/usr/local/bin
export PATH=$GOBIN:$PATH
export PATH=$PATH:/opt/Programs/flutter/bin
export PATH="$PATH":"$HOME/.pub-cache/bin"
export PATH="$PATH":"/var/lib/snapd/snap/bin"
export PATH="$PATH":"$HOME/.local/bin"
export PATH="$PATH":"/opt/Programs/gn"
export PATH="$PATH":"$HOME/.npm-global/bin"
export PATH="$PATH":"$HOME/Qt/Tools/QtCreator/bin"
export ZSH="/home/${USER}/.oh-my-zsh"
export CHROME_EXECUTABLE="/usr/bin/chromium"
export FREEPLANE_USE_UNSUPPORTED_JAVA_VERSION=1
export SDCV_PAGER='less --quit-if-one-screen -RX'
export BROWSER='/usr/bin/vivaldi-stable'
export EDITOR='/usr/bin/nvim'

eval "$(starship init zsh)"

#Enable spell checker
setopt correct
export SPROMPT="Correct %R to %r? [Yes, No, Abort, Edit] "


EDITOR=vim
HISTSIZE=10000
SAVEHIST=10000
# Basic auot/tab complete
autoload -U compinit
autoload zmv
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# vim mode
bindkey -v
export KEYTIMEOUT=1
autoload -U colors && colors

[ -f "$HOME/.aliasrc" ] && source "$HOME/.aliasrc"
source <(fzf --zsh)
eval "$(zoxide init --cmd cd zsh)"

# Use vim keys in tab complete menu
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
CASE_SENSITIVE="true"

plugins=(
  zsh-completions
  zsh-autosuggestions
  history-substring-search
  sudo
  web-search
  zsh-256color
  zsh-vi-mode
  colored-man-pages
  zsh-universal-package-manager
  zsh-syntax-highlighting
)
autoload -U compinit && compinit


zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
bindkey -s '^o' 'lfcd\n'

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# Load aliases and shortcuts if existent.
[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc"
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

# Load zsh-syntax-highlighting; should be last.
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

source $ZSH/oh-my-zsh.sh

